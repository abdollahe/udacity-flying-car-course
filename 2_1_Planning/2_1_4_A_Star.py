# In the name of Allah
# ====================================
# This code implements the breadth search with cost function
# for a 2D grid that represents the surrounding 
# environment for a robotic agent
from queue import PriorityQueue , Queue
import numpy as np
from enum import Enum
from queue import Queue
import math

# imports for performance testing
import datetime
import random

# Define the start and goal states
start = (0,0)
goal = (9,11)

# Define the grid as a matrix which zeros show empty space and 
# ones show the obstacles in the gird 
grid = np.array([
    [0, 1, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 1, 1, 0 , 0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0 , 0, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 1, 0 , 1, 1, 0, 1, 0, 1],
    [0, 1, 0, 1, 0, 0 , 0, 1, 1, 0, 0, 1],
    [0, 1, 0, 1, 0, 0 , 0, 0, 0, 1, 0, 1],
    [0, 1, 0, 1, 1, 0 , 0, 1, 0, 1, 0, 0],
    [0, 1, 0, 1, 0, 0 , 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 0 , 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 1, 0, 0 , 0, 1, 0, 0, 0, 0],
])


# grid = np.array([
#     [0, 1, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0],
#     [0, 1, 0, 0, 0, 0],
#     [0, 0, 0, 1, 1, 0],
#     [0, 0, 0, 1, 0, 0],
# ])




class Action(Enum):

    """
    Class that defines the possible actions the agent can take in the environment
    The Enum has a three member tuple with the first two elements representing the
    changes in position due to action and the third element representing the cost
    for the action.

    """

    LEFT = (0 , -1 , 1)
    
    RIGHT = (0 , 1 , 1)
    
    UP = (-1 , 0 , 1)
    
    DOWN = (1 , 0 , 1)

    UP_RIGHT = (-1 , 1 , 1.4)

    UP_LEFT = (-1 , -1 , 1.4)

    DOWN_RIGHT = (1 , 1 , 1.4)

    DOWN_LEFT = (1 , -1 , 1.4)

    def __str__(self):

        """
        Function that returns a character for the specified action
        in order to print the grid to the console
        """

        if self == self.LEFT:
            return '<'
        elif self == self.RIGHT:
            return '>'
        elif self == self.UP:
            return '^'
        elif self == self.DOWN:
            return 'v'
        elif self == self.UP_RIGHT:
            return '/'
        elif self == self.UP_LEFT:
            return '\\'
        elif self == self.DOWN_LEFT:
            return '/'
        elif self == self.DOWN_RIGHT:
            return '*'


    @property
    def cost(self):
        return self.value[2]

    @property
    def delta(self):
        return (self.value[0] , self.value[1])   



def getPossibleActions(grid , cellPosition):

    """
    Given a grid and the cell position, return the possible 
    actions for that specific cell position
    
    ------------------
    Parameters
    ------
    grid : multi dimensional numpy array
           Grid of the environment to find a path from

    cellPosition : tuple
            Postion in the grid to get the possible actions from    


    ------------------
    Returns
    ------
    List of ENUM Actoin elements -> States all the possible actions for the specified cell         

    """

    possibleActions = [
        Action.DOWN , 
        Action.UP , 
        Action.LEFT , 
        Action.RIGHT , 
        Action.UP_LEFT , 
        Action.UP_RIGHT , 
        Action.DOWN_LEFT , 
        Action.DOWN_RIGHT
        ] 
    
    grid_h_index = grid.shape[0] - 1
    grid_w_index = grid.shape[1] - 1

        
    # Evaluate validity of DOWN action
    new_pos = (cellPosition[0] + Action.DOWN.value[0] , cellPosition[1] + Action.DOWN.value[1])
    
    if( (new_pos[0] > grid_h_index) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.DOWN)
    

    #Evaluate validity of UP action
    new_pos = (cellPosition[0] + Action.UP.value[0] , cellPosition[1] + Action.UP.value[1])

    if( (new_pos[0] < 0) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.UP)


    #Evaluate validity of LEFT action 
    new_pos = (cellPosition[0] + Action.LEFT.value[0] , cellPosition[1] + Action.LEFT.value[1])

    if( (new_pos[1] < 0) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.LEFT)

    #Evaluate validity of RIGHT action 
    new_pos = (cellPosition[0] + Action.RIGHT.value[0] , cellPosition[1] + Action.RIGHT.value[1])

    if( (new_pos[1] > grid_w_index) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.RIGHT)

    # Evaluate validity of UP RIGHT action
    new_pos = (cellPosition[0] + Action.UP_RIGHT.value[0] , cellPosition[1] + Action.UP_RIGHT.value[1])
    
    if( (new_pos[0] < 0) or (new_pos[1] > grid_w_index) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.UP_RIGHT)
    
    # Evaluate validity of UP LEFT action
    new_pos = (cellPosition[0] + Action.UP_LEFT.value[0] , cellPosition[1] + Action.UP_LEFT.value[1])
    
    if( (new_pos[0] < 0) or (new_pos[1] < 0) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.UP_LEFT)

    # Evaluate validity of DOWN RIGHT action
    new_pos = (cellPosition[0] + Action.DOWN_RIGHT.value[0] , cellPosition[1] + Action.DOWN_RIGHT.value[1])
    
    if( (new_pos[0] > grid_h_index) or (new_pos[1] > grid_w_index) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.DOWN_RIGHT)


    # Evaluate validity of UP RIGHT action
    new_pos = (cellPosition[0] + Action.DOWN_LEFT.value[0] , cellPosition[1] + Action.DOWN_LEFT.value[1])
    
    if( (new_pos[0] > grid_h_index) or (new_pos[1] < 0) or (grid[new_pos[0] , new_pos[1]] == 1)):
        possibleActions.remove(Action.DOWN_LEFT)
    
        
    random.shuffle(possibleActions)
    
    return possibleActions

def heuristic(position , goal):

    """
    Given a grid and the cell position, return a value representin the heuristi
    for that position (In this implementation its the euclidean distance between
    the position and the goal)
    
    ------------------
    Parameters
    ------
    position : tuple
           current position in grid 

    goal : tuple
            position of goal in grid


    ------------------
    Returns
    ------
    Number -> Heuristic that represents an estimate of the distance from the current position from the goal

    """

    h = math.sqrt((goal[0] - position[0])**2  + (goal[1] - position[1])**2)
    return h

def visualize_path(grid, path, start):
    """
    Given a grid, path and start position
    return visual of the path to the goal.
    
    'S' -> start 
    'G' -> goal
    'O' -> obstacle
    ' ' -> empty
    """
    # Define a grid of string characters for visualization
    sgrid = np.zeros(np.shape(grid), dtype=np.str)
    sgrid[:] = ' '
    sgrid[grid[:] == 1] = 'O'
    
    pos = start
    # Fill in the string grid
    for a in path:
        da = a.value
        sgrid[pos[0], pos[1]] = str(a)
        pos = (pos[0] + da[0], pos[1] + da[1])
    sgrid[pos[0], pos[1]] = 'G'
    sgrid[start[0], start[1]] = 'S'  
    return sgrid




def getPathCoordinates(start , goal , pathDict): 

    """
    Function to extract the path from the overall path dictionary
    ------------------
    Parameters
    ------
    pathDict : Python Dictionary (key-value pair)
           contains all the visited cells and information on how that cell was visited during search
           A dictionary containing key-value pairs as follows : next_cell : (current_cell , action to go to next_cell)

    start : tuple
            Position defined as the starting point in the grid

    goal : tuple
            position defined as the goas point in the grid        

    ------------------
    Returns
    ------
    List  -> List of actions need to take from start position to goal position
    Number -> Total cost assocciated with the path
    
    """
    
    path = []
    path_cost = 0
    
        
    # retrace steps
    n = goal
    path_cost = pathDict[n][0]
    while pathDict[n][1] != start:
        path.append(pathDict[n][2])
        n = pathDict[n][1]
    path.append(pathDict[n][2])
            
    return path[::-1], path_cost



def aStarSearch(grid , start , goal , heuristicFunction):
    """
    Functon to search the given grid using A star algorithm
    ------------------
    Parameters
    ------
    grid : multi dimensional numpy array
           Grid of the environment to find a path from

    start : tuple
            Position defined as the starting point in the grid

    goal : tuple
            position defined as the goas point in the grid   

    heuristicFunction : function
            A function that calculates the heuristic value for a certain cell             

    ------------------
    Returns
    ------
    Boolean value -> Stating if the function has been able to find a path or not
    Dictionary -> A dictionary containing key-value pairs as follows : next_cell : (current_cell , action to go to next_cell)

    """


    pathFound = False
    q = PriorityQueue()
    visited = set()
    path_dict = {}

    # initialize the data structures
    q.put((0 , start))
    visited.add(start)

    while not q.empty():
        # get current node from queue with highest priority (lowest cost associated with cell)
        item = q.get()
        currentNode = item[1]
        currentCost = item[0]


        if currentNode == goal:
            print("Found path to goal")
            pathFound = True
            break

        else:    
            # get possible actions for current node
            viableActions = getPossibleActions(grid , currentNode)

            for action in viableActions:
                # calculate next node
                nextNode = (currentNode[0] + action.delta[0] , currentNode[1] + action.delta[1])
                newCost = currentCost + action.cost + heuristicFunction(nextNode , goal)

                # check and see if next node has been visisted before
                if(nextNode not in visited):
                    q.put( (newCost , nextNode) )
                    visited.add(nextNode)
                    path_dict[nextNode] = (newCost , currentNode , action)


    return pathFound , path_dict   



def getPathCoordinates(start , goal , pathDict): 


    """
    Function to extract the path from the overall path dictionary
    ------------------
    Parameters
    ------
    pathDict : Python Dictionary (key-value pair)
           contains all the visited cells and information on how that cell was visited during search
           A dictionary containing key-value pairs as follows : next_cell : (current_cell , action to go to next_cell)

    start : tuple
            Position defined as the starting point in the grid

    goal : tuple
            position defined as the goas point in the grid        

    ------------------
    Returns
    ------
    List  -> List of actions need to take from start position to goal position
    Number -> Total cost assocciated with the path
    
    """
    
    path = []
    path_cost = 0
    
        
    # retrace steps
    n = goal
    path_cost = pathDict[n][0]
    while pathDict[n][1] != start:
        path.append(pathDict[n][2])
        n = pathDict[n][1]
    path.append(pathDict[n][2])
            
    return path[::-1], path_cost



## Main part of the code 

start_time = datetime.datetime.now()

status , path_dictionary = aStarSearch(grid , start , goal , heuristic)

if status == True:
    print("Path found extracting path")
    path , pathCost = getPathCoordinates(start,goal,path_dictionary)

    end_time = datetime.datetime.now()

    print("Path extracted successfully - Time is took: " , end_time - start_time)
    print("Path length: " , len(path))
    print("Path cost: " , pathCost)

    sgrid = visualize_path(grid , path , start)
    print(sgrid)

else: 
    print("No paths found")        


  

