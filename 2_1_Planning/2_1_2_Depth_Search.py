# In the name of Allah
# ====================================
# This code implements the depth search 
# for a 2D grid that represents the surrounding 
# environment for a robotic agent

import queue
import numpy as np
from enum import Enum
from queue import Queue

# imports for performance testing
import datetime
import random

# Define the start and goal states
start = (0,0)
goal = (1,11)

# Define the grid as a matrix which zeros show empty space and 
# ones show the obstacles in the gird 

# grid = np.array([
#     [0, 0, 0, 0],
#     [0, 1, 0, 0]
# ])

grid = np.array([
    [0, 1, 0, 0, 0, 0 , 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 1, 1, 0 , 0, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 0, 0 , 0, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 1, 0 , 1, 1, 0, 1, 0, 1],
    [0, 0, 0, 1, 0, 0 , 0, 1, 1, 0, 0, 1],
    [0, 1, 1, 0, 0, 0 , 0, 0, 0, 1, 0, 1],
    [0, 1, 0, 1, 1, 0 , 0, 1, 0, 1, 0, 0],
    [0, 1, 0, 1, 0, 0 , 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 0 , 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 1, 0, 0 , 0, 1, 0, 0, 0, 0],
])


class Action(Enum):

    """
    Class that defines the possible actions the agent can take in the environment

    """

    LEFT = (0 , -1)
    
    RIGHT = (0 , 1)
    
    UP = (-1 , 0)
    
    DOWN = (1 , 0)

    def __str__(self):

        """
        Function that returns a character for the specified action
        in order to print the grid to the console
        """


        if self == self.LEFT:
            return '<'
        elif self == self.RIGHT:
            return '>'
        elif self == self.UP:
            return '^'
        elif self == self.DOWN:
            return 'v'



def getPossibleActions(grid , cellPosition , visitedCells):

    """
    Given a grid and the cell position, return the possible 
    actions for that specific cell position
    
    ------------------
    Parameters
    ------
    grid : multi dimensional numpy array
           Grid of the environment to find a path from

    cellPosition : tuple
            Postion in the grid to get the possible actions from    


    ------------------
    Returns
    ------
    List of ENUM Actoin elements -> States all the possible actions for the specified cell         

    """

    possibleActions = [Action.DOWN , Action.UP , Action.LEFT , Action.RIGHT]
    
    grid_h_index = grid.shape[0] - 1
    grid_w_index = grid.shape[1] - 1

        
    # Evaluate validity of DOWN action
    new_pos = (cellPosition[0] + Action.DOWN.value[0] , cellPosition[1] + Action.DOWN.value[1])
    
    if( (new_pos[0] > grid_h_index) or (grid[new_pos[0] , new_pos[1]] == 1) or (new_pos in visitedCells) ):
        possibleActions.remove(Action.DOWN)
    

    #Evaluate validity of UP action
    new_pos = (cellPosition[0] + Action.UP.value[0] , cellPosition[1] + Action.UP.value[1])

    if( (new_pos[0] < 0) or (grid[new_pos[0] , new_pos[1]] == 1) or (new_pos in visitedCells) ):
        possibleActions.remove(Action.UP)


    #Evaluate validity of LEFT action 
    new_pos = (cellPosition[0] + Action.LEFT.value[0] , cellPosition[1] + Action.LEFT.value[1])

    if( (new_pos[1] < 0) or (grid[new_pos[0] , new_pos[1]] == 1) or (new_pos in visitedCells)):
        possibleActions.remove(Action.LEFT)

    #Evaluate validity of RIGHT action 
    new_pos = (cellPosition[0] + Action.RIGHT.value[0] , cellPosition[1] + Action.RIGHT.value[1])

    if( (new_pos[1] > grid_w_index) or (grid[new_pos[0] , new_pos[1]] == 1) or (new_pos in visitedCells)):
        possibleActions.remove(Action.RIGHT)
    
    random.shuffle(possibleActions)
    
    return  possibleActions


        
def depth_search(grid , start , goal):

    """
    Functon to search the given grid using depth search 
    ------------------
    Parameters
    ------
    grid : multi dimensional numpy array
           Grid of the environment to find a path from

    start : tuple
            Position defined as the starting point in the grid

    goal : tuple
            position defined as the goas point in the grid        

    ------------------
    Returns
    ------
    Boolean value -> Stating if the function has been able to find a path or not
    Dictionary -> A dictionary containing key-value pairs as follows : next_cell : (current_cell , action to go to next_cell)

    """

    stack = []
    visited = set()
    pathDict = {}
    wrongAction = []
    
    searching = True
    foundPath = False


    # initialize the data structures
    stack.append(start)
    visited.add(start)

    while searching == True:
        
        # Get current cell (and push back to have it)
        currentNode = stack.pop()
        #stack.append(currentNode)

        # get list of possible actions for this cell position
        actionsList = getPossibleActions(grid , currentNode , visited)

        if(len(actionsList) == 0):
            if(len(pathDict) > 0):
                temp1 = pathDict.pop(currentNode)
                tAction = temp1[1]
                wrongAction.append(tAction) 
            else:
                searching = False
                foundPath = False      

        else:
            # get first action from the possible actions
            action = actionsList[0]        

            nextNode = (currentNode[0] + action.value[0] , currentNode[1] + action.value[1])

            if(nextNode == goal):
                pathDict[nextNode] = (currentNode , action)
                searching = False
                foundPath = True
            else:
                if(nextNode not in visited):
                    stack.append(currentNode)
                    stack.append(nextNode)
                    pathDict[nextNode] = (currentNode , action)
                    visited.add(nextNode)
                
                else:
                    stack.append(currentNode)
                
              
    return foundPath , pathDict

def getPathCoordinates(start , goal , pathDict): 


    """
    Function to extract the path from the overall path dictionary
    ------------------
    Parameters
    ------
    pathDict : Python Dictionary (key-value pair)
           contains all the visited cells and information on how that cell was visited during search
           A dictionary containing key-value pairs as follows : next_cell : (current_cell , action to go to next_cell)

    start : tuple
            Position defined as the starting point in the grid

    goal : tuple
            position defined as the goas point in the grid        

    ------------------
    Returns
    ------
    List  -> List of actions need to take from start position to goal position
    
    """
    
    # retrace steps
    path = []
    n = goal
    while pathDict[n][0] != start:
        path.append(pathDict[n][1])
        n = pathDict[n][0]
    path.append(pathDict[n][1])
            
    return path[::-1]


def visualize_path(grid, path, start):
    """
    Given a grid, path and start position
    return visual of the path to the goal.
    
    'S' -> start 
    'G' -> goal
    'O' -> obstacle
    ' ' -> empty
    """
    # Define a grid of string characters for visualization
    sgrid = np.zeros(np.shape(grid), dtype=np.str)
    sgrid[:] = ' '
    sgrid[grid[:] == 1] = 'O'
    
    pos = start
    # Fill in the string grid
    for a in path:
        da = a.value
        sgrid[pos[0], pos[1]] = str(a)
        pos = (pos[0] + da[0], pos[1] + da[1])
    sgrid[pos[0], pos[1]] = 'G'
    sgrid[start[0], start[1]] = 'S'  
    return sgrid


start_time = datetime.datetime.now()

status , path_dictionary = depth_search(grid , start , goal)    

if status == True:
    print("Path found extracting path")
    path = getPathCoordinates(start,goal,path_dictionary)

    end_time = datetime.datetime.now()

    print("Path extracted successfully - time it took: " , end_time - start_time)
    print("Path length: " , len(path))  

    sgrid = visualize_path(grid , path , start)
    print(sgrid)

else: 
    print("No paths found")    






    