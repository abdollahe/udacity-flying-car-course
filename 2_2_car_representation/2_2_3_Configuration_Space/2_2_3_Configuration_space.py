# In the name of Allah
# ===========================
# Code for calculating a configuration spcae for 
# a flying  vehicle from a given map

# =========================================================
# =========================================================
import numpy as np
import matplotlib.pyplot as plt
import pathlib


# %matplotlib inline

plt.rcParams["figure.figsize"] = [12 , 12]

file_name = '/colliders.csv'
current_path = str(pathlib.Path(__file__).parent.resolve())
absolute_path = current_path + file_name

# Read in the dat from the CSV file : x-> North , y -> East , Z-> Altitude
data = np.loadtxt(absolute_path , delimiter=',', dtype='Float64' , skiprows=2)

# Static drone altitude -> This parameter helpe create a 2D 
# map since we consider the drone in a specific height
drone_altitude = 10

# Minimum distance required to stay away from an obstacle (meters)
# Thnk of this as padding around the obstacle
safe_distance = 3

def create_grid(data , drone_altitude , safe_distance):
    
    # This parameter holds the minimum horizontal value (x) in the data 
    north_min = np.floor(np.amin(data[: , 0] - data[: , 3]))

    # This paramter holds the maximum horizontal value (x) in the data
    north_max = np.ceil(np.amax(data[: , 0] + data[: , 3]))

    # This parameter holds the minimum vertical value (y) in the data 
    east_min = np.floor(np.amin( data[: , 1] - data[: , 4]))

    # This paramter holds the maximum vertical value (y) in the data
    east_max = np.ceil(np.amax(data[: , 1] + data[: , 4]))

    # Create and initialize a grid with the shape of (north_max - north_min) X (east_max - eat_min) 
    north_size = int(np.ceil(north_max - north_min))
    east_size = int(np.floor(east_max - east_min))

    grid = np.zeros((north_size , east_size))

    # north_min = np.min(data[:,0])
    # east_min = np.min(data[:,1])


    for i in range(data.shape[0]):
        north_i , east_i , altitude_i , delta_north , delta_east , delta_altitude = data[i,:]
         
        t1 = (north_i + abs(north_min))
        north_start =  int(np.floor(t1 - delta_north - safe_distance))
        
        if(north_start < 0 ):
            north_start = 0

        north_end = int(np.ceil(t1 + delta_north + safe_distance)) 

        t2 = (east_i + abs(east_min))

        east_start = int(np.floor(t2 - delta_east - safe_distance))
        if(east_start < 0):
            east_start = 0
        
        east_end = int(np.ceil(t2 + delta_east + safe_distance))

        altitude_end = altitude_i + delta_altitude + safe_distance

        if(drone_altitude <= altitude_end):
            grid[ north_start:north_end , east_start:east_end] = 1
        else:    
            grid[ north_start:north_end , east_start:east_end] = 0

        

    return grid    
        


new_grid = create_grid(data , drone_altitude , safe_distance)

grid_rows = new_grid.shape[0]
grid_columns = new_grid.shape[1]


start = (100 , 100)
goal = (200 , 200)

if( new_grid[start[0] , start[0]] == 0 ):
    print(new_grid[start[0] , start[1]])

if( new_grid[goal[0] , goal[0]] == 0 ):
    print(new_grid[goal[0] , goal[1]])


# equivalent to
# plt.imshow(np.flip(grid, 0))
# NOTE: we're placing the origin in the lower lefthand corner here
# so that north is up, if you didn't do this north would be positive down
plt.imshow(new_grid, origin='lower') 

plt.xlabel('EAST')
plt.ylabel('NORTH')
plt.show()
