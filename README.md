# Flying Vehicle Code base 










## Notes on git structure

### For any changes to the codebase a new branch is created with the following structure:

### [parent branch name]-[new branch name]

### And for any commit the following structure is suggested:

### commit title
### commit body 

### The suggested commit title structure is as follows:
### [type of commit] : (optional) [Ticket id] : [short explanation (title)]

### The different values for the [type of commit] are:

### 1- Feature -> Designates a Feature
### 2- Bugfix -> Designates a bug fix
### 3- Refactor -> Designates refactoring of the code
### 4- Documentation -> Designates adding documentation and comments to the code base


### The commit body holds a small explanation of the changes made and the reason made in plain english

